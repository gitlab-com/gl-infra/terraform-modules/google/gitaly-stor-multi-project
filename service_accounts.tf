locals {
  # We will grant these roles to the service accounts
  # associated with the multi-project instances on the
  # local project
  multi_project_iam_roles = [
    "roles/logging.logWriter",
    "roles/cloudprofiler.agent",
    "roles/cloudtrace.agent",
  ]

  # We will grant these roles to the service accounts
  # associated with the multi-project instances on the
  # host project (var.host_project)
  multi_project_iam_roles_parent = [
    "roles/pubsub.editor",
    "roles/pubsub.publisher",
    "roles/pubsub.subscriber",
  ]

  # These are the services that require service accounts, a unique
  # service account is created for every service
  multi_project_services = ["gitaly"]

  # Builds a map { "<service>:<project>" => { service = <service>, project = <project> } }
  multi_project_services_projects = {
    for sp in setproduct(local.multi_project_services, keys(local.gcp_projects)) :
    "${sp[0]}:${sp[1]}" => { service = sp[0], project = sp[1] }
  }

  # Builds a map { "<service>:<project>:<role>" => { service = <service>, project = <project>, role = <role> } }
  multi_project_parent_access = {
    for spr in setproduct(local.multi_project_services, keys(local.gcp_projects), local.multi_project_iam_roles_parent) :
    "${spr[0]}:${spr[1]}:${spr[2]}" => { service = spr[0], project = spr[1], role = spr[2] }
  }

  # Builds a map { "<service>:<project>:<role>" => { service = <service>, project = <project>, role = <role> } }
  multi_project_local_access = {
    for spr in setproduct(local.multi_project_services, keys(local.gcp_projects), local.multi_project_iam_roles) :
    "${spr[0]}:${spr[1]}:${spr[2]}" => { service = spr[0], project = spr[1], role = spr[2] }
  }
}

resource "google_service_account" "multi_project" {
  for_each     = local.multi_project_services_projects
  project      = each.value.project
  account_id   = each.value.service
  display_name = "Service account used by ${each.value.service} instances for multi-project"
}

resource "google_storage_bucket_iam_member" "gitaly_multi_project_chef_bootstrap_viewer" {
  for_each = local.multi_project_services_projects
  bucket   = var.chef_bootstrap_bucket_name
  role     = "roles/storage.objectViewer"
  member   = google_service_account.multi_project[each.key].member
}

resource "google_storage_bucket_iam_member" "gitaly_multi_project_secrets_viewer" {
  for_each = local.multi_project_services_projects
  bucket   = "gitlab-${var.environment}-secrets"
  role     = "roles/storage.objectViewer"
  member   = google_service_account.multi_project[each.key].member
}

resource "google_storage_bucket_iam_member" "gitaly_multi_project_repository_backup_admin" {
  for_each = local.multi_project_services_projects
  bucket   = "gitlab-${var.environment}-gitaly-repository-backup"
  role     = "roles/storage.objectAdmin"
  member   = google_service_account.multi_project[each.key].member
}

resource "google_kms_key_ring_iam_member" "gitaly_multi_project_bootstrap" {
  for_each    = local.multi_project_services_projects
  key_ring_id = var.bootstrap_key_ring_id
  role        = var.bootstrap_role
  member      = google_service_account.multi_project[each.key].member
}

resource "google_kms_key_ring_iam_member" "gitaly_multi_project_secrets" {
  for_each    = local.multi_project_services_projects
  key_ring_id = var.secrets_key_ring_id
  role        = var.secrets_role
  member      = google_service_account.multi_project[each.key].member
}

resource "google_project_iam_member" "gitaly_multi_project_parent_access" {
  for_each = local.multi_project_parent_access

  project = var.host_project
  role    = each.value.role
  member  = google_service_account.multi_project["${each.value.service}:${each.value.project}"].member
}

resource "google_project_iam_member" "gitaly_multi_project_local_access" {
  for_each = local.multi_project_local_access

  project = each.value.project
  role    = each.value.role
  member  = google_service_account.multi_project["${each.value.service}:${each.value.project}"].member
}
