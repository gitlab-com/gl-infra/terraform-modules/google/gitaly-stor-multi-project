module "firewall_rules" {
  source       = "terraform-google-modules/network/google//modules/firewall-rules"
  version      = "10.0.0"
  project_id   = var.host_project
  network_name = var.network_self_link

  rules = [
    {
      name        = format("deny-all-egress-%s-%s", local.service_name, var.environment)
      direction   = "EGRESS"
      priority    = 65000
      ranges      = ["0.0.0.0/0"]
      target_tags = [local.service_name]
      deny = [{
        protocol = "tcp"
      }]
    },
    {
      name        = format("allow-internal-network-egress-%s-%s", local.service_name, var.environment)
      direction   = "EGRESS"
      priority    = 2000
      ranges      = ["10.0.0.0/8"]
      target_tags = [local.service_name]
      allow = [{
        protocol = "tcp"
      }]
    },
    # Allow public egress to any TCP port, mirrors are cloned/updated directly by Gitaly, we can't control which ports Git repos will be using.
    {
      name        = format("allow-world-egress-%s-%s", local.service_name, var.environment)
      direction   = "EGRESS"
      ranges      = ["0.0.0.0/0"]
      target_tags = [local.service_name]
      allow = [{
        protocol = "tcp"
      }]
    }
  ]
}
