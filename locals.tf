locals {
  # We need to have the project list to be sorted by `create_time` in a
  # determinstic way so that we can use the index of the keys easily and when
  # new projects are added we aren't order depended and append to the list.
  # This doesn't account for projects being deleted.
  sorted_gitaly_gcp_projects_by_create_time = sort([for p in data.google_projects.gitaly_projects.projects : p.create_time if p.lifecycle_state == "ACTIVE"])

  gcp_projects = {
    for p in [
      for create_time in local.sorted_gitaly_gcp_projects_by_create_time : { # Loop through the sorted create_time and find the matching project.
        for p in data.google_projects.gitaly_projects.projects : p.name => p if p.create_time == create_time
      }
    ] : keys(p)[0] => p[keys(p)[0]] # Get the single project and format it as `project_name = { # project data }` so we can `for_each`.
  }
  service_name = "gitaly"
  gitaly_nodes = {
    for pair in setproduct(keys(local.gcp_projects), keys(var.nodes)) :
    join(":", pair) => merge(var.nodes[pair[1]],
      {
        project = pair[0]
        name    = pair[1]
      },
      lookup(lookup(var.node_overrides, pair[0], {}), pair[1], {})
    )
  }
}
