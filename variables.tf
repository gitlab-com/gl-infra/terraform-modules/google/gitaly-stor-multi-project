variable "environment" {
  type        = string
  description = "Environment name for the host project"
}

variable "host_project" {
  type        = string
  description = "Project ID for the host project"
}

variable "network_self_link" {
  type        = string
  description = "Network self link for the host project"
}

variable "bootstrap_key_ring_id" {
  type        = string
  description = "Bootstrap key for Chef"
}

variable "bootstrap_role" {
  type        = string
  description = "Bootstrap role for Chef"
}

variable "nodes" {
  type        = map(any)
  description = "Node data for multi-project Gitaly nodes"
  default     = {}
}

variable "node_overrides" {
  type        = map(any)
  description = "Overrides for multi-project Gitaly nodes"
  default     = {}
}

variable "secrets_key_ring_id" {
  type        = string
  description = "GKMS key ring"
}

variable "secrets_role" {
  type        = string
  description = "GKMS secrets role"
}

variable "chef_bootstrap_bucket_name" {
  type        = string
  description = "Bucket for validator PEM, needed for Chef"
}

variable "vault_service_account_email" {
  type        = string
  description = "Service account email to allow for the impersonation of the package-fetcher service account by Vault"
  default     = null
}
