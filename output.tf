output "gcp_projects" {
  value = local.gcp_projects
}

output "google_service_account_multi_project" {
  value = google_service_account.multi_project
}

output "gitaly_nodes" {
  value = local.gitaly_nodes
}

output "package_fetcher_service_account_name" {
  value = google_service_account.package_fetcher.name
}
