resource "google_compute_shared_vpc_service_project" "gitaly" {
  for_each        = local.gcp_projects
  host_project    = var.host_project
  service_project = each.key
}
