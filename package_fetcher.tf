# In order to fetch Omnibus packages from the gitlab-com-pkgs-release bucket
# a service account is created that can be impersonated by the service
# accounts associated with the Gitaly VMs
#
# The package-fetcher service account is given access to the
# gitlab-com-pkgs-release bucket in the gitlab-com-pkgs
# Terraform environment

resource "google_service_account" "package_fetcher" {
  project      = var.host_project
  description  = "Service account created in the host project, used for reading objects from the gitlab-com-pkgs-release bucket"
  display_name = "package-fetcher"
  account_id   = "package-fetcher"
}

resource "google_service_account_iam_member" "package_fetcher_vault" {
  count              = var.vault_service_account_email != null ? 1 : 0
  service_account_id = google_service_account.package_fetcher.name
  role               = "roles/iam.serviceAccountTokenCreator"
  member             = "serviceAccount:${var.vault_service_account_email}"
}

resource "google_service_account_iam_member" "package_fetcher" {
  for_each           = local.multi_project_services_projects
  service_account_id = google_service_account.package_fetcher.name
  role               = "roles/iam.serviceAccountTokenCreator"
  member             = google_service_account.multi_project[each.key].member
}

resource "google_service_account_iam_member" "package_fetcher_sa_user" {
  for_each           = local.multi_project_services_projects
  service_account_id = google_service_account.multi_project[each.key].name
  role               = "roles/iam.serviceAccountUser"
  member             = google_service_account.multi_project[each.key].member
}
