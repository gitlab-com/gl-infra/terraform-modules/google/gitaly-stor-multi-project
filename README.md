# Gitaly Storage Multi-Project Terraform Module

## What is this?

This module provisions the support resources necessary for running Gitaly in multiple projects.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.5 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.59 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.59 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_firewall_rules"></a> [firewall\_rules](#module\_firewall\_rules) | terraform-google-modules/network/google//modules/firewall-rules | 10.0.0 |

## Resources

| Name | Type |
|------|------|
| [google_compute_shared_vpc_service_project.gitaly](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_shared_vpc_service_project) | resource |
| [google_kms_key_ring_iam_member.gitaly_multi_project_bootstrap](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_key_ring_iam_member) | resource |
| [google_kms_key_ring_iam_member.gitaly_multi_project_secrets](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/kms_key_ring_iam_member) | resource |
| [google_project_iam_member.gitaly_multi_project_local_access](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_project_iam_member.gitaly_multi_project_parent_access](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/project_iam_member) | resource |
| [google_service_account.multi_project](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account.package_fetcher](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account) | resource |
| [google_service_account_iam_member.package_fetcher](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [google_service_account_iam_member.package_fetcher_sa_user](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [google_service_account_iam_member.package_fetcher_vault](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/service_account_iam_member) | resource |
| [google_storage_bucket_iam_member.gitaly_multi_project_chef_bootstrap_viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.gitaly_multi_project_repository_backup_admin](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_storage_bucket_iam_member.gitaly_multi_project_secrets_viewer](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/storage_bucket_iam_member) | resource |
| [google_projects.gitaly_projects](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/projects) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_bootstrap_key_ring_id"></a> [bootstrap\_key\_ring\_id](#input\_bootstrap\_key\_ring\_id) | Bootstrap key for Chef | `string` | n/a | yes |
| <a name="input_bootstrap_role"></a> [bootstrap\_role](#input\_bootstrap\_role) | Bootstrap role for Chef | `string` | n/a | yes |
| <a name="input_chef_bootstrap_bucket_name"></a> [chef\_bootstrap\_bucket\_name](#input\_chef\_bootstrap\_bucket\_name) | Bucket for validator PEM, needed for Chef | `string` | n/a | yes |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment name for the host project | `string` | n/a | yes |
| <a name="input_host_project"></a> [host\_project](#input\_host\_project) | Project ID for the host project | `string` | n/a | yes |
| <a name="input_network_self_link"></a> [network\_self\_link](#input\_network\_self\_link) | Network self link for the host project | `string` | n/a | yes |
| <a name="input_node_overrides"></a> [node\_overrides](#input\_node\_overrides) | Overrides for multi-project Gitaly nodes | `map(any)` | `{}` | no |
| <a name="input_nodes"></a> [nodes](#input\_nodes) | Node data for multi-project Gitaly nodes | `map(any)` | `{}` | no |
| <a name="input_secrets_key_ring_id"></a> [secrets\_key\_ring\_id](#input\_secrets\_key\_ring\_id) | GKMS key ring | `string` | n/a | yes |
| <a name="input_secrets_role"></a> [secrets\_role](#input\_secrets\_role) | GKMS secrets role | `string` | n/a | yes |
| <a name="input_vault_service_account_email"></a> [vault\_service\_account\_email](#input\_vault\_service\_account\_email) | Service account email to allow for the impersonation of the package-fetcher service account by Vault | `string` | `null` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_gcp_projects"></a> [gcp\_projects](#output\_gcp\_projects) | n/a |
| <a name="output_gitaly_nodes"></a> [gitaly\_nodes](#output\_gitaly\_nodes) | n/a |
| <a name="output_google_service_account_multi_project"></a> [google\_service\_account\_multi\_project](#output\_google\_service\_account\_multi\_project) | n/a |
| <a name="output_package_fetcher_service_account_name"></a> [package\_fetcher\_service\_account\_name](#output\_package\_fetcher\_service\_account\_name) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
