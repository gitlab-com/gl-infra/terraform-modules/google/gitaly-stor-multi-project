data "google_projects" "gitaly_projects" {
  filter = "labels.type:gitaly AND labels.environment:${var.environment}"
}
